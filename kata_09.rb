class regra
  attr_accessor :produto, :valor, :qtd_produto
  
  def initialize(produto, valor, qtd_produto=1)
    @produto = produto
    @valor = valor
    @qtd_produto = qtd_produto
  end

end


class Conta

  def initialize(promocoes=[])
    @promocoes = promocoes
    @scan_dos_produtos = Hash.new #Criando chave para validação de Qtd de produto(Hash)
  end
  
  

 # Classe responsável por incrementar a quantidade encontrada de um mesmo produto 
 
  def scan(item)
  
  if  (@scan_dos_produtos.has_key? item) 
  
   (@scan_dos_produtos[item] += 1) 
	else 
   (@scan_dos_produtos[item] = 1)
  end 
  
  # Classe 

  def total
    Conta_total = 0
    @scan_dos_produtos.each do |item, qtd_produto|
      promocoes = get_promocoes_de_produto(item)

      while qtd_produto > 0
        regra = get_best_regra(promocoes, qtd_produto)
        Conta_total += regra.valor
        qtd_produto -= regra.qtd_produto
      end
      
    end
    Conta_total
  end

  private
  

  
#Dentro da lista de promoções , seleciona as regras para aquele produto e ordena eles a partir da qauntidade de produto, ou seja, se a lista possui:
#regra(produto: produto1, qtd_produto: 1)
#regra(produto: produto1, qtd_produto: 2)
#regra(produto: produto2, qtd_produto: 1)

  
  def get_promocoes_de_produto(produto)
    @promocoes.select{ |regra| regra.produto == produto }.sort_by{ |r| r.qtd_produto }.reverse!
  end

  
  
  #seleciona dentro da lista de regras, quais qauntidade  menor igual ao do produto em questão e pega o primeiro item da lista para retornar
  
  
  def get_best_regra(promocoes, qtd_produto)
    return promocoes.select{ |regra| regra.qtd_produto <= qtd_produto }.first
  end

end

